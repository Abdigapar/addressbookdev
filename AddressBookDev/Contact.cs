﻿namespace AddressBookDev
{
    public class Contact
    {
        public Contact(int id, string firstName, string lastName, string number)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Number = number;
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Number { get; set; }
    }
}