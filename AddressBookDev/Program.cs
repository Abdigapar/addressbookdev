﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AddressBookDev
{
    class Program
    {
        private static readonly List<Contact> Contacts = new List<Contact>();
        static void Main(string[] args)
        {
            var again = true;
            Contacts.Add(new Contact(1, "Noyan", "Abdigapar", "8-702-334-56-56"));
            Contacts.Add(new Contact(2, "Al-Farabi", "Rakhimzhanov", "8-707-808-80-80"));
            Contacts.Add(new Contact(3, "Nurzhan", "Mukhamedkali", "8-701-211-11-22"));
            Contacts.Add(new Contact(4, "Al-Farabi", "Mukhamedkali", "8-800-33-44-56"));

            GetAll();

            while (again)
            {
                Console.WriteLine("_________________________");

                Console.WriteLine("Введите команду \n1)Добавить новый контакт \n2)Удалить контакт \n3)Поиск контакта по имени \n4)Сортировать контакты");

                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Add();
                        break;
                    case "2":
                        DeleteContact();
                        break;
                    case "3":
                        SearchContactName();
                        break;
                    case "4":
                        SortContact();
                        break;
                }

                GetAll();

                Console.WriteLine("-------------------------------");
                Console.WriteLine("Хотите продолжить работу? (y/n)");

                Console.WriteLine("Для выхода , введите (e)");

                var yesOrNo = Console.ReadLine()?.ToLower();
                if (yesOrNo == "e")
                {
                    again = false;
                }
            }
            Console.Read();
        }

        static void GetAll()
        {
            Console.WriteLine("Список контактов");
            foreach (var contact in Contacts)
            {
                Console.WriteLine($"{contact.Id}| {contact.FirstName} {contact.LastName}  {contact.Number}");
            }
        }

        static void Add()
        {
            //Добавление нового контакта на последнее Id 
            int lastId = Contacts.LastOrDefault().Id;

            Console.WriteLine("Введите имя");
            var name = Console.ReadLine();

            Console.WriteLine("Введите фамилию");
            var lastName = Console.ReadLine();

            Console.WriteLine("Введите номер телефона");
            var number = Console.ReadLine();

            var newContact = new Contact(++lastId, name, lastName, number);

            Contacts.Add(newContact);
        }

        static void DeleteContact()
        {
            Console.WriteLine("Для удаление контакта, введите его ID");
            var id = Convert.ToInt32(Console.ReadLine());

            var searchId = Contacts.FirstOrDefault(c => c.Id == id);

            if (searchId == null)
            {
                Console.WriteLine("Нету контакта по такому ID");
            }
            else
            {
                Contacts.Remove(searchId);
            }
        }

        static void SearchContactName()
        {
            Console.WriteLine("Для поиска, введите имя контакта");
            var name = Console.ReadLine();

            var searchName = Contacts.Where(c => c.FirstName == name);

            Console.WriteLine("Поиск завершен");
            foreach (var contact in searchName)
            {
                Console.WriteLine($"{contact.Id}| {contact.FirstName} {contact.LastName}  {contact.Number}");
            }
        }

        static void SortContact()
        {
            Console.WriteLine("Сортировать контакты");

            var sort = Contacts.OrderBy(c => c.FirstName);

            foreach (var contact in sort)
            {
                Console.WriteLine($"{contact.Id}| {contact.FirstName} {contact.LastName}  {contact.Number}");
            }
        }
    }
}
